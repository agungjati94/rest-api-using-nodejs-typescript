Setup basic project

1. mkdir learn-nodets
2. cd learn-nodets
3. npm init -y
4. npm i typescript tsc-watch
5. Let’s change scripts section in package.json with this:
    >  "scripts": {
   "dev": "tsc-watch --onSuccess \"node ./dist/server.js\""
}

6. npx tsc --init --moduleResolution node --resolveJsonModule --target es6 --noImplicitAny --sourceMap --lib dom,es2017 --outDir dist
7. mkdir src && touch ./src/server.ts
8. Let’s write a sum function and console.log it every second. Add this in the server.ts file,
    >  const sum = (a: number, b: number) => a + b;
setInterval(() => console.log(sum(2, 3)), 1000);

9. npm run dev

Setup rest api

1. npm install routing-controllers
2. npm install reflect-metadata
3. npm install express body-parser multer
4. npm install -D @types/express @types/body-parser @types/multer
5. npm install class-transformer class-validator
6. Its important to set these options in tsconfig.json file of your project:
    >  {
    "emitDecoratorMetadata": true,
    "experimentalDecorators": true
}